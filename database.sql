-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: cua-hang-thue-xe-mai
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `doi_tac`
--

LOCK TABLES `doi_tac` WRITE;
/*!40000 ALTER TABLE `doi_tac` DISABLE KEYS */;
/*!40000 ALTER TABLE `doi_tac` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `dong_xe`
--

LOCK TABLES `dong_xe` WRITE;
/*!40000 ALTER TABLE `dong_xe` DISABLE KEYS */;
INSERT INTO `dong_xe` VALUES (1,'sedan','Sedan'),(2,'hatchback','Hatchback'),(3,'suv','xe thể thao đa dụng'),(4,'crossover','Crossover'),(5,'mvp','xe đa dụng'),(6,'coupe','xe thể thao'),(7,'convertible ','xe mui trần'),(8,'pickup','xe bán tải'),(9,'limousine','Limousine'),(10,'test','test junit');
/*!40000 ALTER TABLE `dong_xe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `hoa_don_ky_gui_xe`
--

LOCK TABLES `hoa_don_ky_gui_xe` WRITE;
/*!40000 ALTER TABLE `hoa_don_ky_gui_xe` DISABLE KEYS */;
/*!40000 ALTER TABLE `hoa_don_ky_gui_xe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `hoa_don_thue_xe`
--

LOCK TABLES `hoa_don_thue_xe` WRITE;
/*!40000 ALTER TABLE `hoa_don_thue_xe` DISABLE KEYS */;
INSERT INTO `hoa_don_thue_xe` VALUES (2,NULL,NULL,0,0,1,21),(3,NULL,NULL,0,0,1,26),(4,NULL,NULL,0,0,1,21),(5,NULL,NULL,0,0,1,21),(6,NULL,NULL,0,0,1,26),(7,NULL,NULL,0,0,1,21);
/*!40000 ALTER TABLE `hoa_don_thue_xe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `hop_dong_ky_gui_xe`
--

LOCK TABLES `hop_dong_ky_gui_xe` WRITE;
/*!40000 ALTER TABLE `hop_dong_ky_gui_xe` DISABLE KEYS */;
/*!40000 ALTER TABLE `hop_dong_ky_gui_xe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `hop_dong_thue_xe`
--

LOCK TABLES `hop_dong_thue_xe` WRITE;
/*!40000 ALTER TABLE `hop_dong_thue_xe` DISABLE KEYS */;
INSERT INTO `hop_dong_thue_xe` VALUES (1,1,1,NULL,NULL),(2,2,2,NULL,NULL),(3,7,3,NULL,NULL),(4,7,4,NULL,NULL),(5,3,5,NULL,NULL),(6,4,6,NULL,NULL),(7,8,7,NULL,NULL),(8,10,8,NULL,NULL),(9,14,9,NULL,NULL),(10,18,10,NULL,NULL),(11,18,11,NULL,NULL),(12,18,11,NULL,NULL),(13,11,3,'2019-09-19','2019-12-19'),(14,11,3,'2019-09-19','2019-12-19'),(15,11,3,'2019-09-19','2019-12-19'),(16,11,4,'2019-09-19','2019-12-19'),(17,11,5,'2019-09-19','2019-12-19'),(18,7,5,'2019-06-19','2019-12-19'),(19,2,5,'2018-12-01','2019-12-19'),(20,7,5,'2018-12-01','2019-12-19'),(21,2,6,'2018-12-01','2019-12-19'),(22,11,7,'2019-09-19','2019-12-19');
/*!40000 ALTER TABLE `hop_dong_thue_xe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `khach_hang`
--

LOCK TABLES `khach_hang` WRITE;
/*!40000 ALTER TABLE `khach_hang` DISABLE KEYS */;
INSERT INTO `khach_hang` VALUES (1,'20','hanoi20',1),(2,'21','hanoi21',1),(3,'22','hanoi22',1),(4,'23','hanoi23',1),(5,'24','hanoi24',1),(6,'25','hanoi25',1),(7,'26','hanoi26',1),(8,'27','hanoi27',1),(9,'28','hanoi28',1),(10,'29','hanoi29',1),(11,'30','hanoi30',1),(12,'31','hanoi31',1),(13,'32','testjunit',1),(14,'33','33',1),(15,'34','34',1);
/*!40000 ALTER TABLE `khach_hang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `nguoi`
--

LOCK TABLES `nguoi` WRITE;
/*!40000 ALTER TABLE `nguoi` DISABLE KEYS */;
INSERT INTO `nguoi` VALUES (1,'999','admin',NULL,'admin',NULL,NULL,NULL,NULL),(14,'123','nguyễn','văn','nam',NULL,NULL,NULL,NULL),(15,'124','nguyễn','văn','bình',NULL,NULL,NULL,NULL),(16,'126','nguyễn','văn','nhật',NULL,NULL,NULL,NULL),(17,'121','nguyễn','viết','đức',NULL,NULL,NULL,NULL),(18,'111','phạm','đức','cường',NULL,NULL,NULL,NULL),(19,'211','phạm','quý','đức',NULL,NULL,NULL,NULL),(20,'212','tống','thị','ngọc',NULL,NULL,NULL,NULL),(21,'213','vũ','đức','long',NULL,NULL,NULL,NULL),(22,'214','hạng','','vũ',NULL,NULL,NULL,NULL),(23,'215','hạng',NULL,'lương',NULL,NULL,NULL,NULL),(24,'216','lưu',NULL,'bang',NULL,NULL,NULL,NULL),(25,'217','tiêu',NULL,'hà',NULL,NULL,NULL,NULL),(26,'218','trương','','lương',NULL,NULL,NULL,NULL),(27,'219','quảng',NULL,'trọng',NULL,NULL,NULL,NULL),(28,'220','trương',NULL,'bình',NULL,NULL,NULL,NULL),(29,'221','hàn',NULL,'tín',NULL,NULL,NULL,NULL),(30,'222','ngu',NULL,'cơ',NULL,NULL,NULL,NULL),(31,'223','phạm',NULL,'trương',NULL,NULL,NULL,NULL),(32,'999','junit',NULL,'junit',NULL,NULL,NULL,NULL),(33,'3','','','','','','',''),(34,'222','','','','','','','');
/*!40000 ALTER TABLE `nguoi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `nhan_vien`
--

LOCK TABLES `nhan_vien` WRITE;
/*!40000 ALTER TABLE `nhan_vien` DISABLE KEYS */;
INSERT INTO `nhan_vien` VALUES (1,1,'admin','admin',1,1,'admin1',1),(2,14,'nv1','nv1',1,10000000,'staff1',2),(3,15,'nv2','nv2',1,7000000,'staff2',2);
/*!40000 ALTER TABLE `nhan_vien` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'admin','quản lý'),(2,'staff','nhân viên');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tai_san`
--

LOCK TABLES `tai_san` WRITE;
/*!40000 ALTER TABLE `tai_san` DISABLE KEYS */;
INSERT INTO `tai_san` VALUES (1,'a','a','a'),(2,'s','s','s');
/*!40000 ALTER TABLE `tai_san` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tai_san_hop_dong`
--

LOCK TABLES `tai_san_hop_dong` WRITE;
/*!40000 ALTER TABLE `tai_san_hop_dong` DISABLE KEYS */;
INSERT INTO `tai_san_hop_dong` VALUES (1,7,1),(2,7,2);
/*!40000 ALTER TABLE `tai_san_hop_dong` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `xe`
--

LOCK TABLES `xe` WRITE;
/*!40000 ALTER TABLE `xe` DISABLE KEYS */;
INSERT INTO `xe` VALUES (1,1,NULL,'honda 1','honda','2015',NULL,'29-r1-1111',200000,'2019-01-01','2019-01-30',1),(2,2,NULL,'honda 2','honda','2016',NULL,'29-a2-12345',200000,'2019-02-01','2019-02-28',1),(3,3,NULL,'honda 3','honda','2017',NULL,'11-f5-32445',200000,'2019-03-01','2019-03-30',1),(4,9,NULL,'limousine 1','limousine','2018',NULL,'33-g6-34455',200000,'2019-04-01','2019-01-30',1),(5,9,NULL,'limousine 2','limousine','2019',NULL,'37-k8-54666',200000,'2019-05-01','2019-05-30',1),(6,9,NULL,'limousine 3','limousine','2018',NULL,'99-g5-35534',200000,'2019-06-01','2019-06-30',1),(7,2,NULL,'limousine 4','limousine','2017',NULL,'66-k0-43566',200000,'2019-07-01','2019-07-30',1),(8,4,NULL,'crossover 1','crossover','1995',NULL,'34-6h-435431',200000,'2019-08-01','2019-08-30',1),(9,4,NULL,'crossover 2','crossover','1995',NULL,'34-6h-435433',200000,'2019-09-01','2019-09-30',1),(10,4,NULL,'crossover 3','crossover','1995',NULL,'34-6h-435432',200000,'2019-10-01','2019-10-30',1),(11,5,NULL,'mvp 1','mvp','1995',NULL,'34-6h-34543',200000,'2019-11-01','2019-11-30',1),(12,5,NULL,'mvp 3','mvp','1995',NULL,'34-6h-345432',200000,'2019-12-01','2019-12-30',1),(13,5,NULL,'mvp 4','mvp','1995',NULL,'34-6h-23325',200000,'2019-01-01','2019-05-01',1),(14,8,NULL,'pickup 1','pickup','1996',NULL,'12-fg-435433',200000,'2019-01-01','2019-06-01',1),(15,8,NULL,'pickup 2','pickup','1991',NULL,'54-fg-645676',200000,'2019-01-01','2019-07-01',1),(16,8,NULL,'pickup 3','pickup','1993',NULL,'67-we-435433',200000,'2019-01-01','2019-08-01',1),(17,4,NULL,'crossover 1','crossover','1995',NULL,'34-6h-435433',200000,'2019-01-01','2019-09-01',1),(18,10,NULL,'xe junit 1','junit','2019',NULL,'99-mm-900000',200000,'2019-01-01','2019-11-01',1);
/*!40000 ALTER TABLE `xe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'cua-hang-thue-xe-mai'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-19  0:27:44
