/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.KhachHang;
import connect.DBConnect;
import java.awt.List;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lamvi
 */
public class KhachHangDAO {
    private static KhachHangDAO khachHangDAO;
    public static KhachHangDAO instance(){
        if(khachHangDAO == null){
            khachHangDAO = new KhachHangDAO();
        }
        return khachHangDAO;
    }
    
    private final Connection conn = DBConnect.instance();
    private PreparedStatement ps;
    public ArrayList<KhachHang> search(String key)
    {
        try {
            String sql = "SELECT n.*, kh.ma as ma " +
                "FROM khach_hang as kh " +
                "join nguoi as n on kh.nguoi_id = n.id " +
                "where CONCAT(n.ho, n.ten_dem, n.ten) like '%"+key+"%'";
            ps = (PreparedStatement) conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            ArrayList<KhachHang> listKH = new ArrayList<>();
            while (rs.next()) {
                KhachHang kh = new KhachHang();
                kh.setId(rs.getInt("id"));
                kh.setMa(rs.getString("ma"));
                kh.setDient_thoai(rs.getString("dien_thoai"));
                kh.setHo(rs.getString("ho"));
                kh.setTen(rs.getString("ten"));
                kh.setTen_dem(rs.getString("ten_dem"));
                kh.setXa(rs.getString("xa"));
                kh.setHuyen(rs.getString("huyen"));
                kh.setTinh(rs.getString("tinh"));
                kh.setThanh_pho(rs.getString("thanh_pho"));
                listKH.add(kh);
            }
            return listKH;
        } catch (SQLException ex) {
            Logger.getLogger(KhachHangDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new ArrayList<>();
    }
    
    public boolean Add(KhachHang kh)
    {
        try {
            String sql = "INSERT INTO `nguoi` (`dien_thoai`, `ho`, `ten_dem`, `ten`, `xa`, `huyen`, `tinh`, `thanh_pho`,`id`) VALUES (?, ?, ?, ?, ?, ?, ?, ?,?);";
            int id = getMaxId()+1;
            ps = (PreparedStatement) conn.prepareStatement(sql);
            ps.setString(1, kh.getDient_thoai());
            ps.setString(2, kh.getHo());
            ps.setString(3, kh.getTen_dem());
            ps.setString(4, kh.getTen());
            ps.setString(5, kh.getXa());
            ps.setString(6, kh.getHuyen());
            ps.setString(7, kh.getTinh());
            ps.setString(8, kh.getThanh_pho());
            ps.setInt(9, id);
            ps.executeUpdate();
            sql = "INSERT INTO `khach_hang` (`nguoi_id`, `ma`, `rank`) VALUES (?, ?, 1);";
            ps = (PreparedStatement) conn.prepareStatement(sql);
            ps.setInt(1, id);
            ps.setString(2,""+id);
            ps.executeUpdate();
            conn.commit();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(KhachHangDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public int getMaxId()
    {
        try {
            String sql = "SELECT * FROM `cua-hang-thue-xe-mai`.nguoi order by id desc limit 1 ";
            ps = (PreparedStatement) conn.prepareStatement(sql);
            ps.executeQuery();
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                return rs.getInt("id");
            }
        } catch (SQLException ex) {
            Logger.getLogger(KhachHangDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 1;
    }
    
    
    
}
