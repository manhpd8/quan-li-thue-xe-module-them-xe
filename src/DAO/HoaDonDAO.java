/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.HoaDonThueXe;
import Model.HopDongThueXe;
import Model.TaiSan;
import Model.Xe;
import connect.DBConnect;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lamvi
 */
public class HoaDonDAO {
    private static HoaDonDAO hoaDonDAO;
    public static HoaDonDAO instance(){
        if(hoaDonDAO == null){
            hoaDonDAO = new HoaDonDAO();
        }
        return hoaDonDAO;
    }
    private final Connection conn = DBConnect.instance();
    private PreparedStatement ps;
    
    public boolean themHopDongThueXe(HoaDonThueXe hd,ArrayList<HopDongThueXe> listHdtx, ArrayList<TaiSan> listTS){
        try {
            String sql = "INSERT INTO `cua-hang-thue-xe-mai`.`hoa_don_thue_xe` (`id`, `dat_coc`, `nhan_vien_id`, `khach_hang_id`) VALUES (?, ?, ?, ?);";
            int id = getMaxId()+1;
            ps = (PreparedStatement) conn.prepareStatement(sql);
            ps.setInt(1, id);
            ps.setInt(2, hd.getDat_coc());
            ps.setInt(3, hd.getNhan_vien_id());
            ps.setInt(4, hd.getKhach_hang_id());
            ps.executeUpdate();
            sql = "";
            for(HopDongThueXe a : listHdtx){
                sql += "INSERT INTO `cua-hang-thue-xe-mai`.`hop_dong_thue_xe` (`xe_id`, `hoa_don_thue_xe_id`, `ngay_bat_dau`, `ngay_ket_thuc`) "
                        + "VALUES ("+a.getXe_id()+","+id+",'"+a.getNgay_bat_dau()+"','"+a.getNgay_ket_thuc()+"');";
            }
            ps = (PreparedStatement) conn.prepareStatement(sql);
            ps.executeUpdate();
            // them tai san
            sql = "";
            int tsId = getMaxIdTS();
            for(TaiSan b : listTS){
                sql += "INSERT INTO `cua-hang-thue-xe-mai`.`tai_san` (`id`, `ten`, `tinh_trang`, `mo_ta`) VALUES ("+tsId+", '"+b.getTen()+"', '"+b.getTrang_thai()+"', '"+b.getMo_ta()+"');"
                        + "INSERT INTO `cua-hang-thue-xe-mai`.`tai_san_hop_dong` (`hoa_don_thue_xe_id`, `tai_san_id`) VALUES ("+id+", "+tsId+");";
                tsId++;
            }
            System.out.println(sql);
            ps = (PreparedStatement) conn.prepareStatement(sql);
            ps.executeUpdate();
            conn.commit();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(KhachHangDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    public int getMaxId()
    {
        try {
            String sql = "SELECT * FROM hoa_don_thue_xe order by id desc limit 1 ";
            ps = (PreparedStatement) conn.prepareStatement(sql);
            ps.executeQuery();
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                return rs.getInt("id");
            }
        } catch (SQLException ex) {
            Logger.getLogger(HoaDonDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 1;
    }
    
    public int getMaxIdTS()
    {
        try {
            String sql = "SELECT * FROM tai_san order by id desc limit 1 ";
            ps = (PreparedStatement) conn.prepareStatement(sql);
            ps.executeQuery();
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                return rs.getInt("id");
            }
        } catch (SQLException ex) {
            Logger.getLogger(HoaDonDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 1;
    }
}
