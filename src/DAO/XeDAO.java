/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.DongXe;
import Model.Xe;
import connect.DBConnect;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lamvi
 */
public class XeDAO {
    private static XeDAO xeDAO;
    public static XeDAO instance(){
        if(xeDAO == null){
            xeDAO = new XeDAO();
        }
        return xeDAO;
    }
    
    private final Connection conn = DBConnect.instance();
    private PreparedStatement ps;

    public ArrayList<Xe> getListXe(DongXe dx, Date ngayBd, Date ngayKt)
    {
        String sql = "SELECT xe.*FROM xe " +
                    "where xe.san_co = true " +
                    "and dong_id = ? " +
                    "and xe.ngay_bat_dau >= ? and xe.ngay_ket_thuc <= ? ";
        ArrayList<Xe> listXe = new ArrayList<>();
        try {
            ps = (PreparedStatement) conn.prepareStatement(sql);
            ps.setInt(1, dx.getId());
            ps.setDate(2, ngayBd);
            ps.setDate(3, ngayKt);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Xe xe = new Xe();
                xe.setId(rs.getInt("id"));
                xe.setMa(rs.getString("ma"));
                xe.setTen(rs.getString("ten"));
                xe.setBien(rs.getString("bien"));
                xe.setDongXe(dx);
                xe.setHang(rs.getString("hang"));
                xe.setDon_gia(rs.getInt("don_gia"));
                xe.setMo_ta(rs.getString("mo_ta"));
                listXe.add(xe);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBConnect.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listXe;
    }
}
