/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Date;

/**
 *
 * @author lamvi
 */
public class HoaDonThueXe {
    int id;
    String ma;
    Date ngay_bat_dau;
    Date ngay_ket_thuc;
    Date ngay_thanh_toan;
    int dat_coc;
    long don_gia;
    int tien_phat;
    int nhan_vien_id;
    int khach_hang_id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public Date getNgay_bat_dau() {
        return ngay_bat_dau;
    }

    public void setNgay_bat_dau(Date ngay_bat_dau) {
        this.ngay_bat_dau = ngay_bat_dau;
    }

    public Date getNgay_ket_thuc() {
        return ngay_ket_thuc;
    }

    public void setNgay_ket_thuc(Date ngay_ket_thuc) {
        this.ngay_ket_thuc = ngay_ket_thuc;
    }

    public Date getNgay_thanh_toan() {
        return ngay_thanh_toan;
    }

    public void setNgay_thanh_toan(Date ngay_thanh_toan) {
        this.ngay_thanh_toan = ngay_thanh_toan;
    }

    public int getDat_coc() {
        return dat_coc;
    }

    public void setDat_coc(int dat_coc) {
        this.dat_coc = dat_coc;
    }

    public long getDon_gia() {
        return don_gia;
    }

    public void setDon_gia(long don_gia) {
        this.don_gia = don_gia;
    }

    public int getTien_phat() {
        return tien_phat;
    }

    public void setTien_phat(int tien_phat) {
        this.tien_phat = tien_phat;
    }

    public int getNhan_vien_id() {
        return nhan_vien_id;
    }

    public void setNhan_vien_id(int nhan_vien_id) {
        this.nhan_vien_id = nhan_vien_id;
    }

    public int getKhach_hang_id() {
        return khach_hang_id;
    }

    public void setKhach_hang_id(int khach_hang_id) {
        this.khach_hang_id = khach_hang_id;
    }
}
