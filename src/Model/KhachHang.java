/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author lamvi
 */
public class KhachHang extends Nguoi{
    private String ma;

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }
    private int rank;
    public Object[] toObjectSearch(){
        return new Object[]{id,ma,dient_thoai,ho+" "+ten_dem+" "+ten,xa, huyen,tinh,thanh_pho};
    }
    
    public String getHoTen(){
        return ho + " "+ ten_dem+ " "+ ten;
    }
    
    public String getDiaChi(){
        return xa+", "+huyen+", "+tinh+", "+thanh_pho;
    }
}
