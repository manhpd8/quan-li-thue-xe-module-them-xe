/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Date;

/**
 *
 * @author lamvi
 */
public class Xe {
    protected int id;
    protected String ma;
    protected String ten;
    protected String hang;
    protected String doi;
    protected String mo_ta;
    protected String bien;
    protected int don_gia;
    protected Date ngay_bat_dau;
    protected Date ngay_ket_thuc;
    protected boolean san_co;

    public int getDon_gia() {
        return don_gia;
    }

    public void setDon_gia(int don_gia) {
        this.don_gia = don_gia;
    }

    public Date getNgay_bat_dau() {
        return ngay_bat_dau;
    }

    public void setNgay_bat_dau(Date ngay_bat_dau) {
        this.ngay_bat_dau = ngay_bat_dau;
    }

    public Date getNgay_ket_thuc() {
        return ngay_ket_thuc;
    }

    public void setNgay_ket_thuc(Date ngay_ket_thuc) {
        this.ngay_ket_thuc = ngay_ket_thuc;
    }

    public boolean isSan_co() {
        return san_co;
    }

    public void setSan_co(boolean san_co) {
        this.san_co = san_co;
    }
    protected DongXe dongXe;

    public DongXe getDongXe() {
        return dongXe;
    }

    public void setDongXe(DongXe dongXe) {
        this.dongXe = dongXe;
    }
    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getHang() {
        return hang;
    }

    public void setHang(String hang) {
        this.hang = hang;
    }

    public String getDoi() {
        return doi;
    }

    public void setDoi(String doi) {
        this.doi = doi;
    }

    public String getMo_ta() {
        return mo_ta;
    }

    public void setMo_ta(String mo_ta) {
        this.mo_ta = mo_ta;
    }

    public String getBien() {
        return bien;
    }

    public void setBien(String bien) {
        this.bien = bien;
    }
    
    public Object[] toObjectSearch(){
        return new Object[]{ten,bien,dongXe.ten,hang,don_gia,mo_ta};
    }
    
    public Object[] toObjectHoaDon(){
        return new Object[]{ten,bien,don_gia,mo_ta};
    }
}
