/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Date;

/**
 *
 * @author lamvi
 */
public class HopDongThueXe {
    int id;
    int xe_id;
    int hoa_don_thue_id;
    Date ngay_bat_dau;
    Date ngay_ket_thuc;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getXe_id() {
        return xe_id;
    }

    public void setXe_id(int xe_id) {
        this.xe_id = xe_id;
    }

    public int getHoa_don_thue_id() {
        return hoa_don_thue_id;
    }

    public void setHoa_don_thue_id(int hoa_don_thue_id) {
        this.hoa_don_thue_id = hoa_don_thue_id;
    }

    public Date getNgay_bat_dau() {
        return ngay_bat_dau;
    }

    public void setNgay_bat_dau(Date ngay_bat_dau) {
        this.ngay_bat_dau = ngay_bat_dau;
    }

    public Date getNgay_ket_thuc() {
        return ngay_ket_thuc;
    }

    public void setNgay_ket_thuc(Date ngay_ket_thuc) {
        this.ngay_ket_thuc = ngay_ket_thuc;
    }
}
